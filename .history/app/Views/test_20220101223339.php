<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="easyui/themes/icon.css">
    <script type="text/javascript" src="easyui/jquery.min.js"></script>
    <script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h2>Basic CRUD Application</h2>
    <p>Click the buttons on datagrid toolbar to do crud actions.</p>

    <table id="dg" title="My Users" class="easyui-datagrid" style="width:550px;height:250px" url="<?= base_url(); ?>/list" toolbar="#toolbar" rownumbers="true" fitColumns="true" singleSelect="true">
        <thead>
            <tr>
                <th field="nim" width="50">First Name</th>
                <th field="nama" width="50">Last Name</th>
            </tr>
        </thead>
    </table>
    <div id="toolbar">
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="createMahasiswa()">New User</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editMahasiswa()">Edit User</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="deleteMahasiswa()">Remove User</a>
    </div>


    <div id="dlg" class="easyui-dialog" style="width:400px;height:280px;padding:10px 20px" closed="true" buttons="#dlg-buttons">
        <div class="ftitle">User Information</div>
        <form id="fm" method="post" novalidate>
            <div class="fitem">
                <label>First Name:</label>
                <input id="nim" name="nim" class="easyui-textbox" required="true">
            </div>
            <div class="fitem">
                <label>Last Name:</label>
                <input id="nama" name="nama" class="easyui-textbox" required="true">
            </div>
            <div class="fitem">
                <label>Last Name:</label>
                <input id="foto" name="foto" class="easyui-textbox" required="true">
            </div>

        </form>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser()" style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
    </div>

    <script>
        $(document).ready(function() {



            var row = $('#dg').datagrid('getSelected');
            if (row) {


                $('#dlg').dialog('open').dialog('setTitle', 'Edit User');
                $('#fm').form('load', row);
                url = '<?= base_url(); ?>/updatemahasiswa?id=' + row.id;
            }
        });

        function deleteMahasiswa() {
            var row = $('#dg').datagrid('getSelected');
            if (row) {
                $.messager.confirm('Confirm', 'Are you sure you want to destroy this user?', function(r) {
                    if (r) {
                        $.post('<?= base_url(); ?>/deletemahasiswa', {
                            id: row.id
                        }, function(result) {
                            if (result.success) {
                                $('#dg').datagrid('reload'); // reload the user data
                            } else {
                                $.messager.show({ // show error message
                                    title: 'Error',
                                    msg: result.errorMsg
                                });
                            }
                        }, 'json');
                    }
                });
            }
        }

        function saveUser() {

            var nim = $('#nim').val();
            var nama = $('#nama').val();
            var foto = $('#foto').val();

            console.log(foto);

            $.post(url, {
                nim: nim,
                nama: nama,
            }, function(result) {
                if (result.success) {
                    $('#dg').datagrid('reload'); // reload the user data
                    $('#dlg').dialog('close'); // close the dialog

                } else {
                    $.messager.show({ // show error message
                        title: 'Error',
                        msg: result.errorMsg
                    });
                }
            }, 'json');


        }

        function editMahasiswa() {
            var row = $('#dg').datagrid('getSelected');

            if (row) {

                $('#dlg').dialog('open').dialog('setTitle', 'Edit User');
                $('#fm').form('load', row);
                url = '<?= base_url(); ?>/updatemahasiswa?id=' + row.id;
            } else {
                $.messager.alert('Warning', 'Pilih salah satu data!');
            }
        }

        function createMahasiswa() {
            $('#dlg').dialog('open').dialog('setTitle', 'New User');
            $('#fm').form('clear');
            url = '<?= base_url(); ?>/savemahasiswa';



        }
    </script>
    <style type="text/css">
        #fm {
            margin: 0;
            padding: 10px 30px;
        }

        .ftitle {
            font-size: 14px;
            font-weight: bold;
            padding: 5px 0;
            margin-bottom: 10px;
            border-bottom: 1px solid #ccc;
        }

        .fitem {
            margin-bottom: 5px;
        }

        .fitem label {
            display: inline-block;
            width: 80px;
        }

        .fitem input {
            width: 160px;
        }
    </style>
</body>

</html>