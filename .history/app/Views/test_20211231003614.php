<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="easyui/themes/icon.css">
    <script type="text/javascript" src="easyui/jquery.min.js"></script>
    <script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <div id="p" class="easyui-panel" style="width:500px;height:200px;padding:10px;" title="My Panel" data-options="iconCls:'icon-save',collapsible:true">
        The panel content
    </div>
    <div id="p" class="easyui-panel" style="width:500px;height:200px;padding:10px;" title="My Panel" data-options="iconCls:'icon-save',collapsible:true">
        The panel content
    </div>
    <script>
        $('#dd').dialog('mymove', {
            left: 200,
            top: 100
        });
        $.extend($.fn.dialog.methods, {
            mymove: function(jq, newposition) {
                return jq.each(function() {
                    $(this).dialog('move', newposition);
                });
            }
        });
    </script>
</body>

</html>