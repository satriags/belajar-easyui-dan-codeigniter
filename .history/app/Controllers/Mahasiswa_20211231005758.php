<?php

namespace App\Controllers;

class Mahasiswa extends BaseController
{
    public function index()
    {

        $mahasiswa = $this->mahasiswa->get()->getResultArray();

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $offset = ($page - 1) * $rows;
        $result = array();

        // $rs = mysql_query("select count(*) from pelanggan");
        // $row = mysql_fetch_row($rs);
        // $result["total"] = $row[0];
        // $rs = mysql_query("select * from pelanggan limit $offset,$rows");

        // $items = array();
        // while ($row = mysql_fetch_object($rs)) {
        //     array_push($items, $row);
        // }
        // $result["rows"] = $items;

        // echo json_encode($result);
        $rows = json_encode($mahasiswa);

        return view('test', $rows);
    }

    public function create()
    {

        $nim = htmlspecialchars($_REQUEST['nim']);
        $nama = htmlspecialchars($_REQUEST['nama']);


        include 'conn.php';

        $result =  $this->mahasiwa->save(
            [
                'nim' => $nim,
                'nama' => $nama
            ]
        );

        if ($result) {
            echo json_encode(array('success' => 'Success Insert'));
        } else {
            echo json_encode(array('errorMsg' => 'Some errors occured.'));
        }
    }

    public function update()
    {
        $id = htmlspecialchars($_REQUEST['id']);
        $nim = htmlspecialchars($_REQUEST['nim']);
        $nama = htmlspecialchars($_REQUEST['nama']);

        $result =  $this->mahasiwa->set(
            [
                'nim' => $nim,
                'nama' => $nama
            ]
        )->where(['id' => $id])->update();

        if ($result) {
            echo json_encode(array('success' => 'Success Update'));
        } else {
            echo json_encode(array('errorMsg' => 'Some errors occured.'));
        }
    }

    public function delete()
    {
        $id = htmlspecialchars($_REQUEST['id']);


        $result =  $this->mahasiwa->where(['id' => $id])->delete();

        if ($result) {
            echo json_encode(array('success' => 'Success Delete'));
        } else {
            echo json_encode(array('errorMsg' => 'Some errors occured.'));
        }
    }
}
