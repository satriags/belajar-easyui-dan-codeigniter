<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {

        $data = [
            'file' => $this->mahasiswa->getResultArray(),
        ];
        return view('test', $data);
    }
}
