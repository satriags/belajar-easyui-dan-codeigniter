<?php

namespace App\Controllers;

class Mahasiswa extends BaseController
{
    public function index()
    {

        $data = [
            'file' => json_encode($this->mahasiswa->get()->getResultArray()),
        ];
        return view('test', $data);
    }
}
