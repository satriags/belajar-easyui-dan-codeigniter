<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {

        $data = [
            'data' => $this->mahasiswa->getResultArray(),
        ];
        return view('test', $data);
    }
}
